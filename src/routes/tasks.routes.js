const { Router } = require('express')
const router = Router()
const {
  renderNewTaskForm,
  filterByProjet,
  postNewTask,
  tasksLog,
  renderEditTaskForm,
  postEditedTask,
  deleteTask
} = require('../controllers/tasks.controllers')

const { isAuthenticated } = require('../helpers/auth')

//List tasks
router.get('/tasks/allTasks', isAuthenticated, tasksLog)
router.get('/tasks/tasksbyProject/:project', isAuthenticated, filterByProjet)

//Create tasks
router.get('/tasks/newTask', isAuthenticated, renderNewTaskForm)
router.post('/tasks/newTask', isAuthenticated, postNewTask)

//Edit tasks
router.get('/tasks/editor/:id', isAuthenticated, renderEditTaskForm)
router.put('/tasks/editor/:id', isAuthenticated, postEditedTask)

//Delete tasks
router.delete('/tasks/delete/:id', isAuthenticated, deleteTask)

module.exports = router
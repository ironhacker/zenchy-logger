const { Schema, model } = require('mongoose')

const TaskSchema = new Schema ({
  title: { type: String, required: true },
  project: { type: String, required: true },
  description: { type: String, required: true },
  taskNumber: { type: String, required: true },
  difficult: { type: String, required: false },
  state: { type: String, required: true },
  user: { type: String, required: true }
},
{
  timestamps: true
})

module.exports = model ('Task', TaskSchema)

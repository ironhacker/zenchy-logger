const express = require('express')
const morgan = require('morgan')
const exphbs = require('express-handlebars')
const path = require('path')
const methodOverride = require('method-override')
const session = require('express-session')
const passport = require('passport')

//Initialize
const app = express()
require('./config/passport')

//Settings
app.set('port', process.env.PORT || 9000)
app.set('views', path.join(__dirname, 'views'))
app.engine('.hbs',exphbs({
  defaultLayout:'main',
  layoutsDir: path.join(app.get('views'), 'layouts'),
  partialsDir: path.join(app.get('views'), 'partials'),
  extname: '.hbs'
}))
app.set('view engine', '.hbs')


//Middlewares
app.use(express.urlencoded({extended: false}))
app.use(morgan('dev'))
app.use(methodOverride('_method'))
app.use(session({
  secret:'secret',
  resave:true, 
  saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())

//Globals
app.use((req, res, next) => {
  if(req.session){
    res.locals.user = req.user
  }
  next()
})
//Routes
app.use(require('./routes/index.routes'))
app.use(require('./routes/tasks.routes'))
app.use(require('./routes/users.routes'))

//Static
app.use(express.static(path.join(__dirname, 'public')))

module.exports = app

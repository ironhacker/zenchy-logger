const usersCtrl = {};
const passport = require('passport')
const User = require('../models/User')

usersCtrl.renderSignUpForm = (req, res) => {
  res.render('users/signup');
};

usersCtrl.signup = async (req, res) => {
  const errors = [];
  const { name, email, password, confirmpassword } = req.body;
  if (password != confirmpassword) {
    errors.push({ text: 'Passwords do not match' });
  }
  if (password.length < 4) {
    errors.push({ text: 'password too short' });
  }
  if (errors.length > 0) {
    res.render('users/signup', { errors, name, email })
  }
  else {
    const emailUser = await User.findOne({ email: email })
    if (emailUser) {
      errors.push({ text: 'Email used, try a new one!' })
      res.render('users/signup', { errors, name, email })
    } else {
      const newUser = new User({ name, email, password })
      newUser.password = await newUser.encryptPassword(password)
      await newUser.save()
      res.redirect('/users/signin')
    }
  }
  console.log(errors)
};

usersCtrl.renderSignInForm = (req, res) => {
  res.render('users/signin');
};

usersCtrl.signin = passport.authenticate('local', {
  failureRedirect: '/users/signin',
  successRedirect: '/tasks/allTasks',
  //failureFlash: true
})

usersCtrl.logout = (req, res) => {
  req.logout()
  res.redirect('/users/signin')
};

module.exports = usersCtrl;

const tasksController = {}
const Task = require('../models/Task')

//All tasks
tasksController.tasksLog = async (req, res) => {
  let tasks = await Task.find({ user: req.user.id })
  let projects = []
  tasks.map(task => {
   if(!projects.includes(task.project)){
    projects.push(task.project)
   }
  })
  let showProjects = true
  res.render('tasks/allTasks', { tasks, projects, showProjects })
}

//Tasks by project
tasksController.filterByProjet = async (req, res) => {
  let tasksTotal = await Task.find({ user: req.user.id })
  let projects = []
  tasksTotal.map(task => {
   if(!projects.includes(task.project)){
    projects.push(task.project)
   }
  })
  let tasks = await Task.where({project: req.params.project})
  res.render('tasks/allTasks', { tasks, projects })

}

//New tasks
tasksController.renderNewTaskForm = (req, res) => {
  res.render('tasks/newTask')
}
tasksController.postNewTask = async (req, res, next) => {
  const { title, description, difficult, project, state, taskNumber } = req.body
  const newTask = new Task({ title, description, difficult, project, state, taskNumber })
  newTask.user = req.user.id
  await newTask.save()
  res.redirect('/tasks/allTasks')
}

//Edit tasks
tasksController.renderEditTaskForm = async (req, res) => {
  const task = await Task.findById(req.params.id)
  if (task.user != req.user.id) {
    console.log('ERROR')
    return res.redirect('/tasks/allTasks')
  }
  res.render('tasks/editTaskForm', { task })
}

tasksController.postEditedTask = async (req, res) => {
  const { title, description, difficult, project, state, taskNumber } = req.body
  await Task.findByIdAndUpdate(req.params.id, { title, description, difficult, project, state, taskNumber })
  res.redirect('/tasks/allTasks')
}

//Delete tasks
tasksController.deleteTask = async (req, res) => {
  await Task.findByIdAndDelete(req.params.id)
  res.redirect('/tasks/allTasks')
}

module.exports = tasksController
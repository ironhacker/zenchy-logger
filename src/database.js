const mongoose = require('mongoose')
require('dotenv').config()

// const {MON_LOC,MON_COL} = process.env
// const MONGO_URL = `mongodb://${MON_LOC}/${MON_COL}`
const {MONGO_URL} = process.env


mongoose.connect(MONGO_URL, {
  useCreateIndex: true, 
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(db => console.log('We are connected to database!!'))
  .catch(error => console.log('Something has been worng... sorry ', error))
